#include <Arduino.h>

/* special address description flags for the CAN_ID */
#define CAN_EFF_FLAG 0x80000000U /* EFF/SFF is set in the MSB */
#define CAN_RTR_FLAG 0x40000000U /* remote transmission request */
#define CAN_ERR_FLAG 0x20000000U /* error message frame */

/* valid bits in CAN ID for frame formats */
#define CAN_SFF_MASK 0x000007FFU /* standard frame format (SFF) */
#define CAN_EFF_MASK 0x1FFFFFFFU /* extended frame format (EFF) */
#define CAN_ERR_MASK 0x1FFFFFFFU /* omit EFF, RTR, ERR flags */

#define CL_ID (sizeof("12345678##1"))
#define CL_DATA sizeof(".AA")
/* CAN FD ASCII hex short representation with DATA_SEPERATORs */
#define CL_CFSZ (2*CL_ID + 64*CL_DATA)

#define hex_asc_upper_lo(x)	hex_asc_upper[((x) & 0x0F)]
#define hex_asc_upper_hi(x)	hex_asc_upper[((x) & 0xF0) >> 4]

const char hex_asc_upper[] = "0123456789ABCDEF";
static inline void put_hex_byte(char *buf, uint8_t byte) {
	buf[0] = hex_asc_upper_hi(byte);
	buf[1] = hex_asc_upper_lo(byte);
}

static inline void _put_id(char *buf, int end_offset, uint16_t id) {
	/* build 3 (SFF) or 8 (EFF) digit CAN identifier */
	while (end_offset >= 0) {
		buf[end_offset--] = hex_asc_upper[id & 0xF];
		id >>= 4;
	}
}

#define put_sff_id(buf, id) _put_id(buf, 2, id)
#define put_eff_id(buf, id) _put_id(buf, 7, id)

// BEGIN CAN BUS
#include <SPI.h>
#include "mcp_can.h"

const int SPI_CS_PIN = 10;
MCP_CAN CAN(SPI_CS_PIN);

unsigned char flagRecv = 0;
unsigned char len = 0;
unsigned char canMsgBuf[32];
char str[20];
// END CAN BUS


void readFromSerial(char buffer[], int length) {
	int len = -1;
	while((len = Serial.readBytesUntil('\n', buffer, length) < 0)) {
		// wait
	}
}

void initFilters(INT32U filter[]) {
	  Serial.println("init filters");
		CAN.init_Filt(0, 0, filter[0]);                          // there are 6 filter in mcp2515
		CAN.init_Filt(1, 0, filter[1]);                          // there are 6 filter in mcp2515

		CAN.init_Filt(2, 0, filter[2]);                          // there are 6 filter in mcp2515
		CAN.init_Filt(3, 0, filter[3]);                          // there are 6 filter in mcp2515

		CAN.init_Filt(4, 0, filter[4]);
		CAN.init_Filt(5, 0, filter[5]);
		Serial.println("filters done");
}

void initMasks(INT32U masks[]) {
	Serial.println("init masks");
	CAN.init_Mask(0, 0, masks[0]);                         // there are 2 mask in mcp2515, you need to set both of them
	CAN.init_Mask(1, 0, masks[1]);
	Serial.println("masks done");
}


void setup() {
    Serial.begin(115200);

    int counter = 0;
    int status = -1;
    while ((CAN_OK != (status = CAN.begin(CAN_100KBPS))) && counter < 10) {
      counter++;
      Serial.println("Init failed");
      Serial.println("trying again in 100ms");
      delay(100);
    }

    if (status == CAN_OK) {
      Serial.println("CAN BUS Shield init ok!");

      // attachInterrupt(0, MCP2515_ISR, FALLING); // attach interrupt routine
      attachInterrupt(digitalPinToInterrupt(2), MCP2515_ISR, FALLING);
    }
    else {
			Serial.println("CAN Bus Shield init FAILED! EOL");
      while (true) {
        // wait for wdr
      }
    }
}

void MCP2515_ISR() {
    flagRecv = 1;
}

int listen = 0;
char userInput;
unsigned char stmp[8] = {0, 1, 2, 3, 4, 5, 6, 7};
INT8U error = -1;
void loop() {
  // check if get data
  if(listen & flagRecv) {
    // clear flag
    flagRecv = 0;
    // iterate over all pending messages
    // If either the bus is saturated or the MCU is busy,
    // both RX buffers may be in use and reading a single
    // message does not clear the IRQ conditon.
    char outBuf[CL_CFSZ];
    int i, offset;
    while (CAN_MSGAVAIL == CAN.checkReceive()) {
      // Serial.println("Msg available!");
      // read data,  len: data length, buf: data buf
      CAN.readMsgBuf(&len, canMsgBuf);
      // Serial.print("Msg from ID: ");
      // Serial.println(CAN.getCanId());
      if (CAN.getCanId() & (CAN.checkError() > 0)) {
    		put_eff_id(outBuf, CAN.getCanId() & (CAN_ERR_MASK | CAN_ERR_FLAG));
    		outBuf[8] = '#';
    		offset = 9;
    	}
      else if (CAN.getCanId() & CAN_EFF_FLAG) {
    		put_eff_id(outBuf, CAN.getCanId() & CAN_EFF_MASK);
    		outBuf[8] = '#';
    		offset = 9;
    	}
      else {
    		put_sff_id(outBuf, CAN.getCanId() & CAN_SFF_MASK);
    		outBuf[3] = '#';
    		offset = 4;
    	}

      if (CAN.m_nRtr) {
        // remote transmit request RTR
				Serial.printf("RTR detected!\n");
        outBuf[offset++] = 'R';
        outBuf[offset] = 0;
      }

      for (i = 0; i < len; i++) {
    		put_hex_byte(outBuf + offset, canMsgBuf[i]);
    		offset += 2;
    	}

      outBuf[offset] = 0;
			Serial.printf("%10ld", micros());
			Serial.println(outBuf);
    }
  }

  if (listen & ((error = CAN.checkError()) > 0)) {
    if (error == 8) {
      Serial.print("Receive Buffer Full! Interrupt cleared.");
    }
    else {
      Serial.print("Error: ");
      Serial.println(error);
    }
    error = -1;
  }

  while(Serial.available()) {
   userInput = Serial.read();
   Serial.print("userInput:");
   Serial.println(userInput);

	 if (userInput == '0') {
		 detachInterrupt(digitalPinToInterrupt(2));
		 while ((userInput = Serial.read()) < 1) {
			 // wait
		 }

		 INT32U filters[6];
		 INT32U masks[2];
		 int setFilter = 0, setMasks = 0;
		 switch (userInput) {
			 case 'b':
					Serial.print("Filters then Masks in hex: ");
					masks[0] 		= 0x7F00;
					filters[0]	= 0x3C00;
					filters[1]	= 0x3D00;
					masks[1] 		= 0x7FF0;
					filters[2]	= 0x6820;
					filters[3]	= 0xCC20;
					filters[4]	= 0xC6A0;
					filters[5]	= 0x0000;
					// while (Serial.readBytes(filters, 6) < 0) { /* wait */ }
					// while (Serial.readBytes(masks, 3) < 0) { /* wait */ }
					setFilter = 1;
					setMasks = 1;
					listen = 1;
					break;
				case 'c':
					// clear mask and filters
					Serial.print("Filters then Masks in hex: ");
					masks[0] 		= 0x0000;
					masks[1] 		= 0x0000;
					filters[0]	= 0x0000;
					filters[1]	= 0x0000;
					filters[2]	= 0x0000;
					filters[3]	= 0x0000;
					filters[4]	= 0x0000;
					filters[5]	= 0x0000;
					setFilter = 1;
					setMasks = 1;
					listen = 0;
					break;
			 default:
					// toggle listen
					listen = !listen;
				break;
		 }

		 if (setFilter) {
			 initFilters(filters);
			 setFilter = 0;
		 }
		 if (setMasks) {
			 initMasks(masks);
			 setMasks = 0;
		 }
		 attachInterrupt(digitalPinToInterrupt(2), MCP2515_ISR, FALLING);
	 }
   else if (userInput=='1') {
     Serial.println("send msg to 341/342");
		 for (int i = 0x341; (i-0x341)<50; ++i) {
			 Serial.printf("Probe ID {%X} \n", i);
			 byte out1[8] = { 'H', '4', 'C', 'K', '3', 'D', 0x20, 0x20 };
	     byte out2[8] = {'B', 'Y', 0x20, 'S', 'I', 'M', 'O', 'N' };
			 CAN.sendMsgBuf(i, 0, 0, 8, out1);
	     CAN.sendMsgBuf(i+1, 0, 0, 8, out2);
	     delay(50);
	 	}
  }
	else if (userInput == '2') {
		Serial.printf("RTR brute force\n detachInterrupt from pin#2\n");
		detachInterrupt(digitalPinToInterrupt(2));
		byte out[1] = { ' ' };

		INT32U mask0 = 0xE000;
		CAN.init_Mask(0, 0, mask0);

		INT32U filter0 = 0x7000;
		CAN.init_Filt(0, 0, filter0);

		for (int id = 0x300; id <= 0x4FF; id++) {
			Serial.printf("Probe ID {%X} \n", id);
			if (CAN_OK == CAN.sendMsgBuf(id, 1, 0, 0, out)) {
				delay(200);
				while (CAN_MSGAVAIL == CAN.checkReceive()) {
					len = 0;
					INT8U result = CAN.readMsgBuf(&len, canMsgBuf);
					char printBuf[32];
					int offset = 0;
					for (int i = 0; i < len; i++) {
		    		put_hex_byte(printBuf + offset, canMsgBuf[i]);
		    		offset += 2;
		    	}
					if (CAN_OK == result) {
						Serial.printf("RTR to {%X} came back with len{%d} \n{%X}#{%s}", id, len, CAN.getCanId(), printBuf);
					}
					else if (CAN_NOMSG == result) {
						Serial.printf("No msg despite CAN_MSGAVAIL! ID: {%X}\n", id);
					}
				}
			}
		}
		INT32U masks[2], filters[6];
		masks[0] = 0x00;
		masks[1] = 0x00;
		for (int i = 0; i < 6; i++) {
			filters[i] = 0x00;
		}
		initFilters(filters);
		initMasks(masks);

		Serial.println("attachInterrupt to pin#2");
		attachInterrupt(digitalPinToInterrupt(2), MCP2515_ISR, FALLING);
	}
	else if (userInput == '3') {
		Serial.print("Give 8 Char word for 1st line: ");
		char inBuf[9];
		int bytes = -1;
		while ((bytes = Serial.readBytesUntil('\n', inBuf, 9)) < 0) {
			// wait
		}
		int i = 0;
		char tmp = inBuf[i];
		do {
			inBuf[i] = toupper(tmp);
			tmp = inBuf[++i];
		} while ('\n' != tmp);

		for (i = 0; i < 5; i++) {
			CAN.sendMsgBuf(0x341, 0, 0, bytes, inBuf);
			delay(50);
		}
	}
	else if (userInput == '4') {
		Serial.print("Give 8 Char word for 1st line: ");
		char inBuf[9];
		int bytes = -1;
		while ((bytes = Serial.readBytesUntil('\n', inBuf, 9)) < 0) {
			// wait
		}
		int i = 0;
		char tmp = inBuf[i];
		do {
			inBuf[i] = toupper(tmp);
			tmp = inBuf[++i];
		} while ('\n' != tmp);

		for (i = 0; i < 5; i++) {
			CAN.sendMsgBuf(0x342, 0, 0, bytes, inBuf);
			delay(50);
		}
	}
	else if (userInput == '5') {
		Serial.print("ID: ");
		char id[3];
		readFromSerial(id, 3);
		Serial.print("DATA[8]: ");
		char data[8];
		readFromSerial(data, 8);

		Serial.print("ID: ");
		Serial.print(id);
		Serial.print(" DATA: ");
		Serial.println(data);

		for (int i=0; i<5; i++) {
			CAN.sendMsgBuf(id, 0, 0, 8, data);
			delay(50);
		}
	}
  }
}
